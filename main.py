import asyncio
import subprocess
from math import radians
from threading import Thread

from prompt_toolkit import Application
from prompt_toolkit.buffer import Buffer
from prompt_toolkit.filters import Condition
from prompt_toolkit.formatted_text import FormattedText
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.layout import Window, BufferControl, VSplit, FormattedTextControl, Layout, HSplit, FloatContainer, \
    Float, WindowAlign
from prompt_toolkit.shortcuts import message_dialog
from prompt_toolkit.styles import Style
from prompt_toolkit.widgets import Dialog, Button, Frame, Box, Label, HorizontalLine, RadioList, MenuContainer, \
    MenuItem, VerticalLine

from telescope.odrivewrappers import *
from telescope.telescope import Telescope
from telescope.utils import go_to_and_wait


def show_dialog(title, body):
    dialog = Dialog(
        title=title,
        body=Label(text=body,
                   dont_extend_height=True)
    )
    float_dialog = Float(dialog)
    root_container.floats.append(float_dialog)
    # app.invalidate()
    # layout.focus(dialog)
    return float_dialog


def show_accept_dialog(title, body, button_text="Okay"):
    def ok_handler():
        if float_dialog in root_container.floats:
            root_container.floats.remove(float_dialog)
            layout.focus(controls)

    dialog = Dialog(
        title=title,
        body=Label(text=body,
                   dont_extend_height=True),
        buttons=[
            Button(text=button_text, handler=ok_handler),
        ]
    )

    float_dialog = Float(dialog)
    root_container.floats.append(float_dialog)
    layout.focus(dialog)


def hide_dialog(dialog):
    if dialog in root_container.floats:
        root_container.floats.remove(dialog)
    layout.focus(controls)
    app.invalidate()


# def main():
#     tel = Telescope()
#     time.sleep(2)
#     tel.init_odrive()
#     while True:
#         option = input("Enter: Disable motors\n[e]nable motors\n[m]anual mode\n[s]tart stellarium client\n[c]alibrate"
#                        "\ns[t]op stellarium client\nset t[i]me\n[q]uit\nEnter command:")
#         if option == '':
#             tel.disable_motors()
#         elif option == 'e':
#             tel.enable_motors()
#         elif option == 'm':
#             tel.disable_motors()
#             tel.odrv.alt_axis.start_current_ff()
#             input('Press Enter to track current position.')
#             tel.odrv.alt_axis.stop_current_ff()
#             tel.enable_motors()
#             tel.track_current()
#         elif option == 's':
#             tel.stellarium_connect()
#         elif option == 't':
#             tel.stellarium_disconnect()
#         elif option == 'c':
#             tel.stellarium_disconnect()
#             tel.disable_motors()
#             tel.calibrate()
#             tel.enable_motors()
#             tel.stellarium_connect()
#         elif option == 'i':
#             date_time = input("Enter current date&time:")
#             subprocess.run(['date', '-s', date_time])
#         elif option == 'q':
#             tel.disable_motors()
#             tel.stellarium_disconnect()
#             break


def get_errors(odrv, clear=False):
    output = ""
    axes = [(name, axis) for name, axis in odrv._remote_attributes.items() if 'axis' in name]
    axes.sort()
    for name, axis in axes:
        output += name + "\n"

        # Flatten axis and submodules
        # (name, remote_obj, errorcode)
        module_decode_map = [
            ('axis', axis, {k: v for k, v in odrive.enums.__dict__.items() if k.startswith("AXIS_ERROR_")}),
            ('motor', axis.motor, {k: v for k, v in odrive.enums.__dict__.items() if k.startswith("MOTOR_ERROR_")}),
            ('fet_thermistor', axis.fet_thermistor,
             {k: v for k, v in odrive.enums.__dict__.items() if k.startswith("THERMISTOR_CURRENT_LIMITER_ERROR")}),
            ('motor_thermistor', axis.motor_thermistor,
             {k: v for k, v in odrive.enums.__dict__.items() if k.startswith("THERMISTOR_CURRENT_LIMITER_ERROR")}),
            ('encoder', axis.encoder,
             {k: v for k, v in odrive.enums.__dict__.items() if k.startswith("ENCODER_ERROR_")}),
            ('controller', axis.controller,
             {k: v for k, v in odrive.enums.__dict__.items() if k.startswith("CONTROLLER_ERROR_")}),
        ]

        # Module error decode
        for name, remote_obj, errorcodes in module_decode_map:
            prefix = ' ' * 2 + name + ": "
            if (remote_obj.error != 0):
                foundError = False
                output += prefix + "Error(s):" + "\n"
                errorcodes_tup = [(name, val) for name, val in errorcodes.items() if 'ERROR_' in name]
                for codename, codeval in errorcodes_tup:
                    if remote_obj.error & codeval != 0:
                        foundError = True
                        output += "    " + codename + "\n"
                if not foundError:
                    output += "    " + 'UNKNOWN ERROR!' + "\n"
                if clear:
                    remote_obj.error = 0
            else:
                output += prefix + "no error" + "\n"


def status_text(telescope):
    return 'Input voltage: &{:.3f}\n{}'.format(telescope.odrv.odrv.vbus_voltage, get_errors(telescope.odrv.odrv))


def disable():
    if tel:
        tel.disable_motors()
        show_accept_dialog("Disabled", "Motors disabled")
    else:
        not_connected_dialog()


def enable():
    if tel:
        tel.enable_motors()
        show_accept_dialog("Enabled", "Motors enabled")
    else:
        not_connected_dialog()


def manual():
    if tel:
        def ok_handler():
            tel.odrv.alt_axis.stop_current_ff()
            tel.enable_motors()
            tel.track_current()
            if float_container in root_container.floats:
                root_container.floats.remove(float_container)
                layout.focus(controls)

        dialog = Dialog(
            title="Manual mode",
            body=Label(text="Press ENTER to track current position", dont_extend_height=True),
            buttons=[
                Button(text="Track position", handler=ok_handler, width=16),
            ])
        float_container = Float(dialog)
        layout.focus(dialog)
        tel.disable_motors()
        tel.odrv.alt_axis.start_current_ff()
        root_container.floats.append(float_container)
        # input('Press Enter to track current position.')
    else:
        not_connected_dialog()


def fine_tune():
    show_accept_dialog("Fine Tune", "Not yet implemented.")


FINE_TUNE_AMOUNTS = [1.0, 0.25, 0.1, 0.025, 0.01]
accuracy_idx = 0
is_fine_tuning = False
@Condition
def fine_tuning_condition():
    return is_fine_tuning

def finetune_finished_handler():
    if finetune_float_dialog in root_container.floats:
        root_container.floats.remove(finetune_float_dialog)
    layout.focus(controls)
    global is_fine_tuning
    is_fine_tuning = False


fine_tune_dialog = Dialog(
    title="Fine calibration",
    body=Label(text="placeholder text",
               dont_extend_height=True),
    buttons=[
        Button(text="Done", handler=finetune_finished_handler),
    ],
)

finetune_float_dialog = Float(fine_tune_dialog)


def update_finetune_dialog_label():
    fine_tune_amt = FINE_TUNE_AMOUNTS[accuracy_idx]
    label_text = FormattedText([
        ('', "Use "), ('#aa2222', "Arrow Keys"), ('', ' to adjust pointing.\n'),
        ('', "Adjustment increment: "), ('#aa2222', "{}".format(fine_tune_amt)), ('#aaaaaa', " deg "),
        ('#aa2222', "- Page Up/Down"), ('', " to adjust.")
    ])
    fine_tune_dialog.body.text = label_text


def fine_tune_calib():
    if tel:
        if tel.tracking:
            global is_fine_tuning
            is_fine_tuning = True
            update_finetune_dialog_label()
            root_container.floats.append(finetune_float_dialog)
            layout.focus(fine_tune_dialog)
        else:
            show_accept_dialog("Not tracking", "Must be tracking an object to fine-tune.")
    else:
        not_connected_dialog()


def connect():
    if tel:
        def connect_thread():
            tel.stellarium_connect(print_output=False)
            hide_dialog(dia)
            show_accept_dialog("Connected", "Successfully connected to Stellarium.")

        dia = show_dialog("Connecting", "Connecting to stellarium...")
        Thread(target=connect_thread, daemon=True).start()
    else:
        not_connected_dialog()


def disconnect():
    if tel:
        tel.stellarium_disconnect()
        show_accept_dialog("Disconnected", "Disconnected from Stellarium.")
    else:
        not_connected_dialog()


def calibrate():
    if tel:
        calib_dialog = show_dialog("Calibrating",
                                   "Point the telescope to an object, then slew to it in Stellarium to calibrate.")

        def calib_thread():
            tel.stellarium_disconnect()
            tel.disable_motors()
            tel.calibrate(print_output=False)
            tel.enable_motors()
            tel.stellarium_connect()
            hide_dialog(calib_dialog)
            show_accept_dialog("Calibrated", "Calibrated telescope successfully")

        Thread(target=calib_thread, daemon=True).start()

    else:
        not_connected_dialog()


# TODO Set time
def set_time():
    date_time = input("Enter current date&time:")
    subprocess.run(['date', '-s', date_time])


def shutdown():
    if tel:
        tel.disable_motors()
        tel.stellarium_disconnect()
    app.exit()


controls = MenuContainer(
    body=Window(),
    menu_items=[
        MenuItem(
            "Stellarium",
            children=[
                MenuItem("Connect to Stellarium", handler=connect),
                MenuItem("Disconnect from Stellarium", handler=disconnect)
            ]
        ),
        MenuItem(
            "Enable/disable motors",
            children=[
                MenuItem("Enable motors", handler=enable),
                MenuItem("Disable motors", handler=disable)
            ]
        ),
        MenuItem("Calibrate", children=[
            MenuItem("Manual calibration", calibrate),
            MenuItem("Fine Tune calibration", handler=fine_tune_calib)
        ]),
        MenuItem("Manual mode", children=[
            MenuItem("Manual movement", handler=manual),
            MenuItem("Fine-tune position", handler=fine_tune)
        ]),
        MenuItem(
            "Shut down & exit",
            handler=shutdown
        )
    ]
)


def focus_controls():
    layout.focus(controls)


tel_status = FormattedTextControl(text='Telescope Status Text')
tracking_status = FormattedTextControl(text='Telescope Tracking Status Text')
status_control = VSplit([Window(tel_status, align=WindowAlign.CENTER, width=24), VerticalLine(),
                         Window(tracking_status, align=WindowAlign.LEFT)])

connect_dialog = Dialog(
    title="Connecting",
    body=Label(text="Connecting to ODrive...", dont_extend_height=True),
    buttons=[
        Button(text="Cancel", handler=shutdown),
    ]
)

connect_float = Float(connect_dialog)

# noinspection PyTypeChecker
content = HSplit([
    # Display the text 'Hello world' on the bottom.
    Frame(title="Status", body=status_control),

    # One window that holds the BufferControl with the default buffer on
    # the bottom.
    Frame(title="Controls", body=controls),
])
root_container = FloatContainer(content, floats=[connect_float])

layout = Layout(root_container)

kb = KeyBindings()


@kb.add('c-q')
def exit_(event):
    """
    Pressing Ctrl-Q will exit the user interface.

    Setting a return value means: quit the event loop that drives the user
    interface and return this value from the `Application.run()` call.
    """
    shutdown()


@kb.add('c-d')
def disable(event):
    if tel:
        tel.disable_motors()

        def ok_handler():
            if float_container in root_container.floats:
                root_container.floats.remove(float_container)
                layout.focus(controls)

        dialog = Dialog(
            title="Motors Disabled",
            body=Label(text="Press ENTER to continue", dont_extend_height=True),
            buttons=[
                Button(text="Okay", handler=ok_handler),
            ])

        float_container = Float(dialog)
        root_container.floats.append(
            float_container  # optionally pass width and height.
        )

        layout.focus(dialog)
    else:
        not_connected_dialog()


@kb.add("pageup")
def increase_amount(event):
    global accuracy_idx
    accuracy_idx = (accuracy_idx + 1) % len(FINE_TUNE_AMOUNTS)
    update_finetune_dialog_label()


@kb.add("pagedown")
def decrease_amount(event):
    global accuracy_idx
    accuracy_idx = accuracy_idx - 1
    if accuracy_idx < 0:
        accuracy_idx += len(FINE_TUNE_AMOUNTS)
    update_finetune_dialog_label()


@kb.add("up", filter=fine_tuning_condition)
def up(event):
    if tel:
        tel.odrv.alt_axis.zero_pos += tel.odrv.alt_axis.deg_to_native_units(FINE_TUNE_AMOUNTS[accuracy_idx])


@kb.add("down", filter=fine_tuning_condition)
def up(event):
    if tel:
        tel.odrv.alt_axis.zero_pos -= tel.odrv.alt_axis.deg_to_native_units(FINE_TUNE_AMOUNTS[accuracy_idx])


@kb.add("left", filter=fine_tuning_condition)
def up(event):
    if tel:
        tel.odrv.az_axis.zero_pos -= tel.odrv.az_axis.deg_to_native_units(FINE_TUNE_AMOUNTS[accuracy_idx])


@kb.add("right", filter=fine_tuning_condition)
def up(event):
    if tel:
        tel.odrv.az_axis.zero_pos += tel.odrv.az_axis.deg_to_native_units(FINE_TUNE_AMOUNTS[accuracy_idx])


def not_connected_dialog():
    def ok_handler():
        if float_container in root_container.floats:
            root_container.floats.remove(float_container)
            layout.focus(controls)

    dialog = Dialog(
        title="Telescope not connected",
        body=Label(text="Press ENTER to continue", dont_extend_height=True),
        buttons=[
            Button(text="Okay", handler=ok_handler),
        ])

    float_container = Float(dialog)
    root_container.floats.append(
        float_container  # optionally pass width and height.
    )
    layout.focus(dialog)


tel: Telescope = None


def init_telescope():
    global tel  # Ack
    tel = Telescope(print_output=False)
    # time.sleep(2)

    if connect_float in root_container.floats:
        root_container.floats.remove(connect_float)

    def ok_handler():
        if float_container in root_container.floats:
            root_container.floats.remove(float_container)
            layout.focus(controls)

        tel.init_odrive(prompt=False)

    dialog = Dialog(
        title="Initialize ODrive",
        body=Label(text="Press ENTER to initialize ODrive & calibrate encoders. WARNING: Telescope will move!"),
        buttons=[
            Button(text="Initialize", handler=ok_handler),
        ])

    float_container = Float(dialog)
    root_container.floats.append(
        float_container  # optionally pass width and height.
    )
    layout.focus(dialog)


def update_status_forever():
    while True:
        if tel:
            tel_status.text = "Connected!\nBattery: {:.2f}v".format(tel.odrv.odrv.vbus_voltage)
            if tel.tracking:
                tracking_status.text = FormattedText([
                    ('', "Tracking\n"),
                    ('#aa2222', "Target:\nRA/Dec (degrees): "),
                    ('#aaaaaa', "{:.4f}, {:.4f}\n".format(*tel.target_ra_dec)),
                    ('#aa2222', "Azimuth/Altitude (degrees): "),
                    ('#aaaaaa', "{:3.4f}, {:2.4f}\n".format(*tel.target_az_alt)),
                    ('', "Telescope position:\n"),
                    ('#aa2222', "Azimuth/Altitude (degrees): "),
                    ('#aaaaaa', "{:3.4f}, {:2.4f}\n".format(tel.odrv.az_axis.position_degrees(),
                                                          tel.odrv.alt_axis.position_degrees())),
                    ('#aa2222', "Pointing error (degrees):   "),
                    ('#aaaaaa', "{:.5f}, {:.5f}".format(tel.odrv.az_axis.position_degrees() - tel.target_az_alt[0],
                                                        tel.odrv.alt_axis.position_degrees() - tel.target_az_alt[1])),
                ])
            else:
                tracking_status.text = FormattedText([
                    ('#aa0000', "Not tracking anything.")
                ])
        else:
            tel_status.text = FormattedText([
                ('#880000', "Not connected")
            ])
            tracking_status.text = FormattedText([
                ('#880000', "Not connected")
            ])
        app.invalidate()
        time.sleep(1)


def init_telescope_threaded():
    layout.focus(connect_dialog)
    Thread(target=init_telescope, daemon=True).start()
    Thread(target=update_status_forever, daemon=True).start()


style = Style.from_dict(
    {
        "window.border": "#888888",
        "shadow": "bg:#222222",
        "menu-bar": "bg:#aaaaaa #222222",
        "menu-bar.selected-item": "bg:#ffffff #000000",
        "menu": "bg:#888888 #ffffff",
        "menu.border": "#aaaaaa",
        "window.border shadow": "#444444",
        # "focused  button": "bg:#880000 #ffffff noinherit",
        # Styling for Dialog widgets.
        "button-bar": "bg:#aaaaff",
    }
)

app = Application(key_bindings=kb, layout=layout, style=style, full_screen=True, mouse_support=True)

app.run(pre_run=init_telescope_threaded)
