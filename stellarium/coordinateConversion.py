import time

import astropy.units as u
from astropy.time import Time
from astropy.coordinates import *

import angles

backyard_location = EarthLocation(lat=42.46793 * u.deg, lon=-83.67882 * u.deg)


class CoordinateConverter:
    def __init__(self, earth_location=backyard_location):
        self.earth_location = earth_location

    def ra_dec_to_alt_az(self, ra, dec, time=None):
        if time is None:
            time = Time.now()
        sky_coord = SkyCoord(ra=ra.d * u.deg, dec=dec.d * u.deg, obstime=time)
        altaz = sky_coord.transform_to(AltAz(obstime=time, location=self.earth_location))

        alt = angles.Angle(d=altaz.alt.value)
        az = angles.Angle(d=altaz.az.value)
        return alt, az

    def alt_az_to_ra_dec(self, alt, az, time=None):
        if time is None:
            time = Time.now()
        sky_coord = SkyCoord(frame=AltAz(obstime=time, location=self.earth_location),
                             alt=alt.d * u.deg, az=az.d * u.deg)

        ra_dec = sky_coord.transform_to(ICRS())
        ra = angles.Angle(d=ra_dec.ra.value)
        dec = angles.Angle(d=ra_dec.dec.value)
        return ra, dec


if __name__ == '__main__':
    conv = CoordinateConverter()
    print(Time.now())
    alt, az = conv.ra_dec_to_alt_az(angles.Angle(h=12), angles.Angle(d=32))  # , Time(1588049897.18282, format='unix'))
    print(alt.d, az.d)
    time.sleep(10)
    print(Time.now())
    alt, az = conv.ra_dec_to_alt_az(angles.Angle(h=12), angles.Angle(d=32))  # , Time(1588049997.18282, format='unix'))
    print(alt.d, az.d)
    ra, dec = conv.alt_az_to_ra_dec(alt, az)
    print(ra.d, dec.d)
