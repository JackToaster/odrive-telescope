import math
import select
import socket
import struct
import time
from threading import Thread, Event

import angles


def angle_from_stellarium(ra, dec, mtime):
    ra = angles.Angle(h=ra * 12.0 / 2147483648)
    dec = angles.Angle(d=dec * 90.0 / 1073741824)

    time_s = mtime / 1000000

    return ra, dec, time_s


def angle_to_stellarium(ra, dec, mtime):
    return (int(ra.h * (2147483648 / 12.0)),
            int(dec.d * (1073741824 / 90.0)),
            int(mtime))


class StellariumClient:
    def __init__(self, host, port):
        self.serverAddress = (host, port)

        # Create a TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.sock.bind(self.serverAddress)
        self.sock.settimeout(600)  # Throws a timeout exception if connections are idle for 10 minutes
        self.sock.listen(1)  # set the socket to listen
        self.connected = False
        self.connection = None

    def handshake(self):
        while True:
            # Wait for a connection
            connection, client_address = self.sock.accept()
            if connection is not None:
                self.connected = True
                self.connection = connection
                # print('Client address: %s' % str(client_address))
                break

    def _receive_data(self):
        try:
            incomingData = None
            ready = select.select([self.connection], [], [])
            if ready[0]:
                incomingData = self.connection.recv(160)
            return incomingData
        except Exception as e:
            print("failed to receive data from Stellarium: %s" % e)

    def receive(self):
        data = self._receive_data()
        try:
            st_time, st_ra, st_dec = struct.unpack('<xxxxQIi', data)
            ra, dec, t = angle_from_stellarium(st_ra, st_dec, st_time)
            return ra, dec, t
        except Exception as e:
            print("failed to decode data from Stellarium: %s" % e)

    def send(self, ra, dec, mtime):
        st_ra, st_dec, st_time = angle_to_stellarium(ra, dec, mtime)

        size = struct.calcsize('<HxxQIii')  # For some reason there's an extra int on the end. IDk why.

        data = struct.pack('<HxxQIii', size, st_time, st_ra, st_dec, 0)

        try:
            #for i in range(10):  # WHY do you have to send the data 10 times? Why are you like this, Stellarium?
                self.connection.send(data)
        except Exception as e:
            print("failed to send data to Stellarium: %s" % e)

    def _receive_loop(self, set_pos_function, get_pos_function, stop_event):
        while not stop_event.is_set():
            received = self.receive()
            if received is not None:
                set_pos_function(*received)
                self.send(*get_pos_function())

    def _update_loop(self, get_pos_function, update_rate, stop_event):
        while not stop_event.is_set():
            time.sleep(1.0 / update_rate)
            self.send(*get_pos_function())

    def run(self, set_pos_function, get_pos_function, update_rate=10):
        self.thread_stop_event = Event()
        receive_thread = Thread(target=self._receive_loop,
                                args=(set_pos_function, get_pos_function, self.thread_stop_event),
                                daemon=True)
        update_thread = Thread(target=self._update_loop,
                               args=(get_pos_function, update_rate, self.thread_stop_event),
                               daemon=True)

        receive_thread.start()
        update_thread.start()

    def stop(self):
        if self.thread_stop_event:
            self.thread_stop_event.set()


if __name__ == '__main__':  # Runs a dummy telescope server like Stellarium's virtual telescope
    client = StellariumClient('localhost', 10001)
    print('Attempting to connect to Stellarium...')
    client.handshake()
    print('Connected!')
    # while True:
    #     received = client.receive()
    #     if received is not None:
    #         print(received[0].hms, received[1].dms, received[2])
    #         client.send(*received)
