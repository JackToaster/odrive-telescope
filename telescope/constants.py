# Constants related to encoders and reductions
import time

servo_epr = 1.0
reduction_ratio = 12.0 / 1.0
edges_per_degree = servo_epr * reduction_ratio / 360.0

# Speed to move the altitude axis while zeroing
altitude_zero_vel = 2.5  # Degrees per second
altitude_zero_trigger_current = 7.0  # amps
altitude_zero_angle = 100

# The rate that the offboard control loops run
offboard_control_loop_time = 1.0 / 50.0


def wait_until(condition):
    while not condition():
        time.sleep(offboard_control_loop_time)


# The threshold to be considered "at" an angle
angle_threshold = 0.1  # degrees

# Whether to use the current feedforward map
current_ff_enabled = False

# How long to wait between feeding axis watchdog timers
watchdog_wait = 0.5  # Seconds

stellarium_host = '0.0.0.0'
stellarium_port = 10001

normal_vel_integrator_gain = 0.002
manual_vel_integrator_gain = 0.04
manual_vel_integrator_limit = 0.009
manual_vel_integrator_decay = 0.75
