import time
from threading import Event, Thread

from astropy.time import Time
from odrive.utils import dump_errors

from stellarium.coordinateConversion import CoordinateConverter, angles
from stellarium.stellariumClient import StellariumClient
from telescope.constants import stellarium_host, stellarium_port, offboard_control_loop_time
from telescope.odrivewrappers import ODriveWrapper
from prompt_toolkit.shortcuts import message_dialog


class Telescope:
    def __init__(self, print_output=True):
        self.odrv = ODriveWrapper(print_output=print_output)
        self.coord_converter = CoordinateConverter()
        self.stellarium = None

        self.tracking = False
        self.track_thread = None
        self.stop_tracking_signal = None

        self.target_ra_dec = (0, 0)
        self.target_az_alt = (0, 0)

    def set_location(self, location):
        self.coord_converter = CoordinateConverter(earth_location=location)

    def init_odrive(self, prompt=True):
        if prompt:
            while True:
                try:
                    input("press Enter to calibrate motors.")
                    break
                except EOFError as e:
                    time.sleep(5)

        self.odrv.calibrate_encoders(print_output=prompt)
        # input("Press Enter to zero altitude axis.")
        self.odrv.zero_altitude_axis(print_output=prompt)

    def stellarium_init(self, print_output=True):
        if print_output:
            print('Connecting to stellarium...')
        self.stellarium = StellariumClient(stellarium_host, stellarium_port)
        self.stellarium.handshake()
        if print_output:
            print('Connected!')

    def stellarium_connect(self, print_output=True):
        if self.stellarium is None:
            self.stellarium_init(print_output=print_output)
        self.stellarium.run(get_pos_function=self.get_pos, set_pos_function=self.track_ra_dec)

    def stellarium_disconnect(self):
        if self.stellarium is not None:
            self.stellarium.stop()

    def enable_motors(self):
        self.odrv.az_axis.enable_closed_loop()
        self.odrv.alt_axis.enable_closed_loop()

    def disable_motors(self):
        self.odrv.disable_motors()
        self.stop_tracking()

    def _track_loop(self, ra, dec):
        self.target_ra_dec = (ra.d, dec.d)
        self.goto(ra, dec)
        while not self.stop_tracking_signal.is_set() \
                and not (self.odrv.alt_axis.at_goto_setpoint() and self.odrv.az_axis.at_goto_setpoint()):
            time.sleep(offboard_control_loop_time)

        while not self.stop_tracking_signal.is_set():
            alt, az = self.coord_converter.ra_dec_to_alt_az(ra, dec)
            self.target_az_alt = (az.d, alt.d)
            self.odrv.alt_axis.set_position_with_ff(alt.d)
            self.odrv.az_axis.go_to_angle(az.d)
            time.sleep(offboard_control_loop_time)

    def stop_tracking(self):
        if self.tracking:
            self.stop_tracking_signal.set()
            self.track_thread.join()
            self.tracking = False

    def track_current(self):
        ra, dec, _ = self.get_pos()
        self.stop_tracking()
        self.stop_tracking_signal = Event()
        self.track_thread = Thread(target=self._track_loop, args=(ra, dec), daemon=True)
        self.track_thread.start()
        self.tracking = True

    def track_ra_dec(self, ra, dec, time=None):
        self.stop_tracking()
        self.stop_tracking_signal = Event()
        self.track_thread = Thread(target=self._track_loop, args=(ra, dec), daemon=True)
        self.track_thread.start()
        self.tracking = True

    def goto(self, ra, dec, time=None):
        alt, az = self.coord_converter.ra_dec_to_alt_az(ra, dec)

        self.odrv.alt_axis.go_to_angle(alt.d)
        self.odrv.az_axis.go_to_angle(az.d)

    def get_pos(self):
        raw_alt = self.odrv.alt_axis.position_degrees()
        alt = angles.Angle(d=max(min(raw_alt, 90), 0))
        az = angles.Angle(d=self.odrv.az_axis.position_degrees())
        # print('alt: %d az: %d'  % (alt.d, az.d))
        ra, dec = self.coord_converter.alt_az_to_ra_dec(alt, az)
        return ra, dec, Time.now().unix

    def calibrate(self, print_output=True):
        if self.stellarium is None:
            self.stellarium_init()

        if print_output:
            print('Point the telescope at an object, then slew to it in Stellarium to calibrate.')

        self.odrv.alt_axis.start_current_ff()
        ra, dec, _ = self.stellarium.receive()
        # print("RA: %d Dec: %d" % (ra.d, dec.d))
        self.odrv.alt_axis.stop_current_ff()
        if ra is not None and dec is not None:
            alt, az = self.coord_converter.ra_dec_to_alt_az(ra, dec)
            self.odrv.alt_axis.set_position_to(alt.d)
            self.odrv.az_axis.set_position_to(az.d)
            if print_output:
                print('Zeroed azimuth axis to %d degrees, alt axis to %d degrees.' % (az.d, alt.d))
            else:
                return az.d, alt.d
        else:
            if print_output:
                print('Failed to receive data from Stellarium.')
            else:
                return None
