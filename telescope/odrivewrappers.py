import threading
import time

import angles
import odrive
from odrive.enums import *
from odrive.utils import dump_errors

from mapFF import get_ff_value
from telescope.constants import altitude_zero_trigger_current, offboard_control_loop_time, edges_per_degree, \
    altitude_zero_vel, \
    altitude_zero_angle, watchdog_wait, normal_vel_integrator_gain, manual_vel_integrator_gain, \
    manual_vel_integrator_limit, manual_vel_integrator_decay
from telescope.utils import at_angle


def deg_to_native_units(degrees):
    return degrees * edges_per_degree


def native_units_to_deg(units):
    return units / edges_per_degree


class AxisWrapper:
    def __init__(self, axis, inverted=False):
        self.axis = axis
        self.zero_pos = 0.0
        self.inverted = inverted
        self.goto_setpoint = 0

        # Create a thread that feeds the watchdog timer
        watchdog_thread = threading.Thread(target=self.feed_watchdog)
        watchdog_thread.setDaemon(True)
        watchdog_thread.start()

    def feed_watchdog(self):
        while True:
            self.axis.watchdog_feed()
            time.sleep(watchdog_wait)

    def deg_to_absolute_native_units(self, degrees):
        if self.inverted:
            native_units = -deg_to_native_units(degrees)
        else:
            native_units = deg_to_native_units(degrees)
        return native_units + self.zero_pos

    def deg_to_native_units(self, degrees):
        if self.inverted:
            native_units = -deg_to_native_units(degrees)
        else:
            native_units = deg_to_native_units(degrees)
        return native_units

    def absolute_native_units_to_deg(self, units):
        absolute_native_units = units - self.zero_pos
        if self.inverted:
            return -native_units_to_deg(absolute_native_units)
        else:
            return native_units_to_deg(absolute_native_units)

    def position_degrees(self):
        return self.absolute_native_units_to_deg(self.axis.encoder.pos_estimate)

    def get_output_current(self):
        if self.inverted:
            return -self.axis.motor.current_control.Iq_setpoint
        else:
            return self.axis.motor.current_control.Iq_setpoint

    def zero_axis_hard_stop(self, velocity, trigger_current, zero_to=0.0, print_output=True):
        if self.inverted:
            velocity = -velocity

        self.axis.controller.config.control_mode = CONTROL_MODE_POSITION_CONTROL
        self.axis.controller.config.input_mode = INPUT_MODE_TRAP_TRAJ

        # Enable closed loop control
        if print_output:
            print("Enabling closed loop")
        self.axis.controller.input_pos = self.axis.encoder.pos_estimate
        self.axis.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL

        # Hold for a sec to settle
        time.sleep(2.0)

        # Get starting position in degrees
        setpoint = native_units_to_deg(self.axis.encoder.pos_estimate)

        # Move slowly until the current trigger is exceeded
        while abs(self.get_output_current()) < trigger_current:
            time.sleep(offboard_control_loop_time)
            setpoint += velocity * offboard_control_loop_time
            # print(deg_to_native_units(setpoint))
            self.axis.controller.input_pos = deg_to_native_units(setpoint)

        self.set_position_to(zero_to)

        self.disable_closed_loop()

    def set_position_to(self, angle):
        if self.inverted:
            self.zero_pos = self.axis.encoder.pos_estimate + deg_to_native_units(angle)
        else:
            self.zero_pos = self.axis.encoder.pos_estimate - deg_to_native_units(angle)

    def enable_closed_loop(self):
        # Reset position setpoint
        self.axis.controller.input_pos = self.axis.encoder.pos_estimate

        # self.axis.controller.config.vel_integrator_gain = normal_vel_integrator_gain

        self.axis.controller.config.control_mode = CONTROL_MODE_POSITION_CONTROL
        self.axis.controller.config.input_mode = INPUT_MODE_TRAP_TRAJ
        self.axis.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL

    def disable_closed_loop(self):
        self.axis.requested_state = AXIS_STATE_IDLE

    def go_to_angle(self, angle):
        self.axis.controller.input_pos = self.deg_to_absolute_native_units(angle)
        self.goto_setpoint = angle

    def at_goto_setpoint(self):
        return at_angle(self.goto_setpoint, self.position_degrees())


def closest_rotation(target, current):
    larger = target
    while larger > target:
        larger -= 360
    while larger < target:
        larger += 360

    smaller = larger - 360

    if abs(larger - current) < abs(smaller - current):
        return larger
    else:
        return smaller


# Azimuth can rotate 360 degrees, so it should always go the shorter distance to the target.
# TODO maybe do this in odrive config?
class AzAxisWrapper(AxisWrapper):
    def go_to_angle(self, angle):
        current_angle = self.position_degrees()
        target_angle = closest_rotation(angle, current_angle)

        self.axis.controller.input_pos = self.deg_to_absolute_native_units(target_angle)
        self.goto_setpoint = target_angle


class AltAxisWrapper(AxisWrapper):
    def __init__(self, axis, inverted=False):
        super().__init__(axis, inverted)
        self.current_thread_event = None
        self.current_thread = None

    def go_to_angle(self, angle):
        # Limit alt axis to -5 to 95 degrees
        angle = min(max(angle, -5), 95)
        self.axis.controller.input_pos = self.deg_to_absolute_native_units(angle)
        self.goto_setpoint = angle

    def set_position_with_ff(self, target):
        current_ff = get_ff_value(self.position_degrees())
        native_target = self.deg_to_absolute_native_units(target)
        self.axis.controller.input_pos = native_target
        self.axis.controller.input_torque = current_ff

    def _current_loop(self, use_velocity):
        self.axis.requested_state = AXIS_STATE_IDLE
        if use_velocity:
            self.axis.controller.config.input_mode = INPUT_MODE_VEL_RAMP
            self.axis.controller.config.control_mode = CONTROL_MODE_VELOCITY_CONTROL
            # self.axis.controller.config.vel_integrator_gain = manual_vel_integrator_gain
        else:
            self.axis.controller.config.input_mode = INPUT_MODE_PASSTHROUGH
            self.axis.controller.config.control_mode = CONTROL_MODE_TORQUE_CONTROL

        self.axis.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL
        while not self.current_thread_event.is_set():
            if use_velocity:
                self.run_current_ff_velocity()

                # If the velocity integrator (Essentially proporional to displacement) is large enough, make it decay
                vel_integrator = self.axis.controller.vel_integrator_torque
                if abs(vel_integrator) > manual_vel_integrator_limit:
                    self.axis.controller.vel_integrator_torque = vel_integrator * manual_vel_integrator_decay
            else:
                self.run_current_ff()
            time.sleep(offboard_control_loop_time)

        # self.axis.controller.config.vel_integrator_gain = normal_vel_integrator_gain

        self.axis.requested_state = AXIS_STATE_IDLE
        self.axis.controller.config.input_mode = INPUT_MODE_TRAP_TRAJ
        self.axis.controller.config.control_mode = CONTROL_MODE_POSITION_CONTROL

    def start_current_ff(self, use_velocity=True):
        self.current_thread_event = threading.Event()
        self.current_thread = threading.Thread(target=self._current_loop, args=[use_velocity], daemon=True)
        self.current_thread.start()

    def stop_current_ff(self):
        self.disable_closed_loop()
        if self.current_thread is not None and self.current_thread_event is not None:
            self.current_thread_event.set()
            self.current_thread.join()

    def run_current_ff(self):
        current_ff = get_ff_value(self.position_degrees())
        self.axis.controller.current_setpoint = current_ff

    def run_current_ff_velocity(self):
        current_ff = get_ff_value(self.position_degrees())
        self.axis.controller.input_vel = 0
        self.axis.controller.input_torque = current_ff


def find_odrive():
    return odrive.find_any()


class ODriveWrapper:
    def __init__(self, print_output=True):
        if print_output:
            print('Searching for odrive...')
        self.odrv = find_odrive()

        if print_output:
            print("Odrive found.")
            print("Odrive.vbus_voltage:" + str(self.odrv.vbus_voltage))

        # Create axis wrappers
        self.az_axis = AzAxisWrapper(self.odrv.axis0)
        self.alt_axis = AltAxisWrapper(self.odrv.axis1, True)

        # This clears the watchdog not fed error
        self.az_axis.axis.watchdog_feed()
        self.alt_axis.axis.watchdog_feed()

        if print_output:
            print('Clearing the following error codes:')
            dump_errors(self.odrv, True)

    def calibrate_encoders(self, print_output=True):
        if print_output:
            print('Calibrating encoders...')
        self.odrv.axis0.requested_state = AXIS_STATE_ENCODER_INDEX_SEARCH
        while self.odrv.axis0.current_state != AXIS_STATE_IDLE:
            time.sleep(0.1)
        self.odrv.axis1.requested_state = AXIS_STATE_ENCODER_INDEX_SEARCH
        while self.odrv.axis1.current_state != AXIS_STATE_IDLE:
            time.sleep(0.1)
        if print_output:
            print('Encoders calibrated')

    def zero_altitude_axis(self, print_output=True):
        if print_output:
            print('Zeroing alitude axis...')
        self.alt_axis.zero_axis_hard_stop(altitude_zero_vel, altitude_zero_trigger_current, altitude_zero_angle, print_output=print_output)

    def disable_motors(self):
        self.odrv.axis0.requested_state = AXIS_STATE_IDLE
        self.odrv.axis1.requested_state = AXIS_STATE_IDLE
