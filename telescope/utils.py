from telescope.constants import wait_until, angle_threshold


def at_angle(angle, target):
    return abs(angle - target) < angle_threshold


def go_to_and_wait(axis, target):
    axis.go_to_angle(target)
    wait_until(lambda: at_angle(axis.position_degrees(), target))