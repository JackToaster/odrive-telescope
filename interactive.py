import subprocess

from odrive.utils import dump_errors

from telescope.telescope import Telescope


from IPython.terminal.embed import InteractiveShellEmbed
from IPython.core.magic import register_line_magic

help_text = '''
        ODrive Telescope Control
        help: Display this text
        status: Print ODrive status
        disable: Disable motors
        enable: Enable motors
        manual: Manual aiming mode
        connect: Connect to stellarium
        disconnect: Disconnect from stellarium
        calibrate: Calibrate to an object with stellarium
        set_time: Set the current system time
        exit() / ^D: Exit and shutdown motors
    '''

ishell = InteractiveShellEmbed(
    banner1=help_text,
    exit_msg='Shutting down motors & exiting...'
)


def get_ipython():
    return ishell


telescope = Telescope()
telescope.init_odrive()


@register_line_magic
def help(line):
    print(help_text)


@register_line_magic
def status(line):
    print('Errors:')
    dump_errors(telescope.odrv.odrv)
    print('Input voltage: &{:.3f}'.format(telescope.odrv.odrv.vbus_voltage))


@register_line_magic
def disable(line):
    telescope.disable_motors()


@register_line_magic
def enable(line):
    telescope.enable_motors()


@register_line_magic
def manual(line):
    telescope.disable_motors()
    telescope.odrv.alt_axis.start_current_ff()
    input('Press Enter to track current position.')
    telescope.odrv.alt_axis.stop_current_ff()
    telescope.enable_motors()
    telescope.track_current()


@register_line_magic
def connect(line):
    telescope.stellarium_connect()


@register_line_magic
def disconnect(line):
    telescope.stellarium_disconnect()


@register_line_magic
def calibrate(line):
    telescope.stellarium_disconnect()
    telescope.disable_motors()
    telescope.calibrate()
    telescope.enable_motors()
    telescope.stellarium_connect()


@register_line_magic
def set_time(line):
    date_time = input("Enter current date&time:")
    subprocess.run(['date', '-s', date_time])


def shutdown():
    telescope.disable_motors()
    telescope.stellarium_disconnect()


del disable, enable, manual, connect, disconnect, calibrate, set_time, help, status


ishell()
shutdown()
