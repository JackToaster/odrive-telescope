import subprocess

from telescope.odrivewrappers import *
from telescope.telescope import Telescope
from telescope.utils import go_to_and_wait


def main():
    tel = Telescope()
    tel.init_odrive()
    while True:
        option = input("Enter: Disable motors\n[e]nable motors\n[q]uit\nEnter command:")
        if option == '':
            tel.odrv.alt_axis.stop_current_ff()
        elif option == 'e':
            tel.disable_motors()
            tel.odrv.alt_axis.start_current_ff()
        elif option == 'q':
            tel.disable_motors()
            break


if __name__ == '__main__':
    main()
